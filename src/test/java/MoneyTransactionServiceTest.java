import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.platform.commons.util.StringUtils;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class MoneyTransactionServiceTest {

    private MoneyTransactionService testInstance;


    private static final double RANDOM_MONEY_AMOUNT = 100;
    private static final double RANDOM_MONEY_AMOUNT_2 = 90;
    private static final double MINIMUM_MONEY_AMOUNT = 0;
    private static final String ACCOUNT_VALUE_NULL_MESSAGE = "Accounts shouldn't be null";
    private static final String ACCOUNT_VALUE_NEGATIVE_MESSAGE = "Money amount should be greater than 0";


    @BeforeEach
    void setUp() {
        testInstance = new MoneyTransactionService();

    }

    @Test
    void shouldReturnMoneyFromOneAccountToAnotherAccount() throws MoneyTransactionService.NotEnoughMoneyException {
        var Account1 = new Account(RANDOM_MONEY_AMOUNT);
        var Account2 = new Account(MINIMUM_MONEY_AMOUNT);

        testInstance.transferMoney(Account1, Account2, RANDOM_MONEY_AMOUNT);

        assertEquals(MINIMUM_MONEY_AMOUNT, Account1.getMoneyAmount());
        assertEquals(RANDOM_MONEY_AMOUNT, Account2.getMoneyAmount());
        assertNotEquals(RANDOM_MONEY_AMOUNT_2, Account2.getMoneyAmount());
    }

    @Test
    void shouldThroughExceptionIfAccountFromIsNull() throws MoneyTransactionService.NotEnoughMoneyException {
        Account Account1 = null;
        Account Account2 = new Account(MINIMUM_MONEY_AMOUNT);
        var exception = assertThrows(IllegalArgumentException.class, () -> testInstance.transferMoney(Account1, Account2, RANDOM_MONEY_AMOUNT_2));
        assertEquals(ACCOUNT_VALUE_NULL_MESSAGE, exception.getMessage());

    }

    @Test
    @DisplayName("Exception Through if Someone try to transact amount less or equal zero")
    void shouldTroughExceptionIfAccountValueIsNegative() {
        Account account1 = new Account();
        Account account2 = new Account();

        var exception = assertThrows(IllegalArgumentException.class, () -> testInstance.transferMoney(account1, account2, MINIMUM_MONEY_AMOUNT));
        assertEquals(ACCOUNT_VALUE_NEGATIVE_MESSAGE, exception.getMessage());
    }

    //    group assertions
    @Test
    void groupedAssertionsExamples() throws MoneyTransactionService.NotEnoughMoneyException {

        // In a grouped assertion all assertions are executed, and all
        // failures will be reported together.
//        GIVEN

        Account account1 = new Account(RANDOM_MONEY_AMOUNT);
        Account account2 = new Account(MINIMUM_MONEY_AMOUNT);


        // WHEN
        testInstance.transferMoney(account1, account2, RANDOM_MONEY_AMOUNT);

//        THEN
        assertAll("money transaction", () -> assertEquals(MINIMUM_MONEY_AMOUNT, account1.getMoneyAmount()), () -> assertEquals(RANDOM_MONEY_AMOUNT, account2.getMoneyAmount()));

    }


    @Test
    void dependentAssertionsExample() {
        // Within a code block, if an assertion fails the
        // subsequent code in the same block will be skipped.

        // GIVEN
        var account1 = new Account(RANDOM_MONEY_AMOUNT);
        var account2 = new Account(MINIMUM_MONEY_AMOUNT);

        assertAll("Money Transaction",
                () -> {
                    // WHEN
                    boolean isTransactionSuccesd = testInstance.transferMoney(account1, account2, RANDOM_MONEY_AMOUNT);
                    assertTrue(isTransactionSuccesd);
                    assertFalse(!isTransactionSuccesd);

                    // Executed only if the previous assertion is valid.
                    assertAll("Money amount is changed on the accounts",
                            () -> assertEquals(MINIMUM_MONEY_AMOUNT, account1.getMoneyAmount()),
                            () -> assertEquals(RANDOM_MONEY_AMOUNT, account2.getMoneyAmount())
                    );
                }
        );
    }


    @Test
    void testWithTimeoutExample() {

        /*
        * The @Timeout annotation allows one to declare that a test, test factory, test template, or lifecycle method should fail if its execution time exceeds a given duration.
        * The time unit for the duration defaults to seconds but is configurable.
         *
        * */

        // GIVEN
        var account1 = new Account(RANDOM_MONEY_AMOUNT);
        var account2 = new Account(MINIMUM_MONEY_AMOUNT);

        assertTimeout(Duration.ofSeconds(1),()->testInstance.transferMoney(account1,account2,RANDOM_MONEY_AMOUNT_2));
    }



    @Test
    @Timeout(value = 10,unit = TimeUnit.MILLISECONDS)
//	@Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    void timeoutNotExceededWithResult() throws MoneyTransactionService.NotEnoughMoneyException {
        // GIVEN
        var account1 = new Account(RANDOM_MONEY_AMOUNT);
        var account2 = new Account(MINIMUM_MONEY_AMOUNT);

        // The following assertion succeeds, and returns the supplied object.

        boolean stateOfTheMethod = assertTimeout(Duration.ofMillis(30), ()-> {
            return testInstance.transferMoney(account1,account2,RANDOM_MONEY_AMOUNT_2);
        } );

        assertTrue(stateOfTheMethod);
    }


    // Parameterized test is not done yet
    @ParameterizedTest
    @ValueSource(ints = {100, 200, 50, -10})
    void parametrizedTestExample(int moneyAmount) throws MoneyTransactionService.NotEnoughMoneyException {
        assumeTrue(moneyAmount > 0, () -> "Money amount can't be negative");

        var account1 = new Account(moneyAmount);
        var account2 = new Account(MINIMUM_MONEY_AMOUNT);

        assertTrue(testInstance.transferMoney(account1, account2, moneyAmount));

    }


}
